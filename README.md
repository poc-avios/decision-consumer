decision consumer
==============================================

Java project that invoke a the DMN execution embedding the Red Hat Decision Manager (drools) libraries.

There are two option to recall the decision project:

- statically from the classpath

```java
    KieContainer kieContainer = kieServices.getKieClasspathContainer();
```

- dynamically from a maven repo

```java
    ReleaseId releaseId = kieServices.newReleaseId("com.example", "decision-kjar", "1.0-SNAPSHOT");
    KieContainer kieContainer = kieServices.newKieContainer(releaseId);
```
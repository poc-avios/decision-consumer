package client;

import org.kie.api.KieServices;
import org.kie.api.builder.ReleaseId;
import org.kie.api.runtime.KieContainer;
import org.kie.dmn.api.core.DMNContext;
import org.kie.dmn.api.core.DMNModel;
import org.kie.dmn.api.core.DMNResult;
import org.kie.dmn.api.core.DMNRuntime;
import org.kie.dmn.api.core.event.AfterEvaluateDecisionTableEvent;
import org.kie.dmn.api.core.event.BeforeEvaluateBKMEvent;
import org.kie.dmn.api.core.event.BeforeEvaluateDecisionEvent;
import org.kie.dmn.api.core.event.DMNRuntimeEventListener;

/**
 * EmbedMain
 */
public class EmbedMain {

    public static void main(String[] args) {

        KieContainer kieContainer = getContainer();

        evaluateDMN(kieContainer);
        // run again after warm-up
        // evaluateDMN(kieContainer);
    }

    private static void evaluateDMN(KieContainer kieContainer) {
        // start the stopwatch
        long start = System.currentTimeMillis();

        // init the runtime
        DMNRuntime dmnRuntime = kieContainer.newKieSession().getKieRuntime(DMNRuntime.class);

        // for debugging purposes add runtme listener
        // addListener(dmnRuntime);

        // input data
        DMNContext dmnContext = dmnRuntime.newContext();

        dmnContext.set("Partner", "VY");
        dmnContext.set("Product", "Ancillary");
        dmnContext.set("Sub Product", "Seat");
        dmnContext.set("Price", "1000");

        // retrieve the model
        String namespace = "http://www.redhat.com/definitions/avios-poc";
        String modelName = "avios";

        DMNModel dmnModel = dmnRuntime.getModel(namespace, modelName);

        // execute
        DMNResult dmnResult = dmnRuntime.evaluateAll(dmnModel, dmnContext);

        // stop watch
        long end = System.currentTimeMillis();

        // print the result
        System.out.println("Result decision: " + dmnResult.getDecisionResultByName("Calculate discount and avios points").getResult());

        System.out.println("elapsed time: " + (end - start));
    }

    private static KieContainer getContainer() {
        KieServices kieServices = KieServices.Factory.get();

        // Retrieve the decision project (kjar) from classpath
        // KieContainer kieContainer = kieServices.getKieClasspathContainer();

        // Dynamically pull the decision project from Maven repo
        ReleaseId releaseId = kieServices.newReleaseId("com.example", "decision-kjar", "1.0-SNAPSHOT");
        KieContainer kieContainer = kieServices.newKieContainer(releaseId);
        return kieContainer;
    }

    private static void addListener(DMNRuntime dmnRuntime) {
        dmnRuntime.addListener(new DMNRuntimeEventListener() {

			@Override
			public void beforeEvaluateDecision(BeforeEvaluateDecisionEvent event) {
				System.out.format(">>> node: '%s' context: %s\n", event.getDecision().getName(), event.getResult().getContext());
			}

			@Override
			public void afterEvaluateDecisionTable(AfterEvaluateDecisionTableEvent event) {
				System.out.format(">>> table: '%s' matched rows: %s\n", event.getDecisionTableName(), event.getMatches());
			}

			@Override
			public void beforeEvaluateBKM(BeforeEvaluateBKMEvent event) {
				System.out.format(">>> node: '%s' context: %s\n", event.getBusinessKnowledgeModel().getName(), event.getResult().getContext());
			}

		});
    }
}